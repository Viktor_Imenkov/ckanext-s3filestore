import ipdb
import base64
import hashlib

import boto3
from boto3.session import Session


# S3 acces credentials
key_id = 'AKIAIGF2Y5I6YAAB75WQ'
access_key = '2eJi58dJkKm6zLReNxS1zTq0XRhQ0f1vK4mDt93k'
bucket = 'myololobuckert213913653'
region = 'eu-west-1'

kms_master_key_id = '7630c91c-229d-43bf-b71b-830b1f0fad0a'


session = Session(aws_access_key_id='AKIAIGF2Y5I6YAAB75WQ',
                  aws_secret_access_key='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',
                  region_name='eu-west-1'
                  )


# test key credentials
# data_key2 = '12345678901234567890123456789012' # 32 bytes long
# key2_base64 = 'MTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTI=' # Base64 encoding of data_key2
# # test md5
# m2 = hashlib.md5()
# m2.update(data_key2)
# key_md5 = m2.digest()
# data_key2_md5_base64 = base64.b64encode(key_md5) # dnF5x6K/8ZZRzpfSlMMM+w==


kms = session.client('kms')
s3 = session.client('s3')

data_key_req = kms.generate_data_key(KeyId=kms_master_key_id, KeySpec='AES_256')
data_key = data_key_req['Plaintext']
data_key_ciphered = data_key_req['CiphertextBlob']

print('plain text: ', data_key)
print('encrypted ket ', data_key_ciphered)

# MD5
# m = hashlib.md5()
# m.update(data_key)
# data_key_md5 = m.digest()
# print('md5: ', data_key_md5)
# print('base64 encoded md5: ', base64.b64encode(data_key_md5))
#
#
# s3.put_object(Bucket=bucket, Body=open('ololo.file', 'r'),
#               Key='ololo.file',
#               SSECustomerAlgorithm='AES256',
#               SSECustomerKey=base64.b64encode(data_key),
#               SSECustomerKeyMD5=base64.b64encode(data_key_md5)
#               )



# decrypted_data_key = kms.decrypt(CiphertextBlob=data_key_ciphered)['Plaintext']
# print('decrypted_data_key', decrypted_data_key)
key_path = 'resources/49c990e1-b99f-4cfa-9ab8-581ff251369f/7i7gtu5pv7g.jpg'
data_key_ciphered_base64 = 'CiASuABobusflY7t547WQ8R8SZ42UDOSoq55AD6njrOxVRKnAQEBAwB4ErgAaG7rH5WO7eeO1kPEfEmeNlAzkqKueQA+p46zsVUAAAB+MHwGCSqGSIb3DQEHBqBvMG0CAQAwaAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAw4t06VyYYMXDnNZk0CARCAO6DyPwcrnVtkMTZgqKUdTxqNqlYDQ3CQQOO4nCMhiXH6x/1BKWhAL2+m3y4NrqDuXlBIvNbKGRDYP/EL'
data_key_ciphered = base64.b64decode(data_key_ciphered_base64)
decrypted_data_key = kms.decrypt(CiphertextBlob=data_key_ciphered)['Plaintext']

# md5
m2 = hashlib.md5()
m2.update(decrypted_data_key)
decrypted_data_key_md5 = m2.digest()
print('decrypted_data_key_md5 : ', decrypted_data_key_md5)

# import ipdb
# ipdb.set_trace()
p1 = str(base64.b64encode(decrypted_data_key))[2:-1]
p2 = str(base64.b64encode(decrypted_data_key_md5))[2:-1]

print(type(p1))
print(type(p2))

response = s3.get_object(Bucket=bucket,
                         Key=key_path,
                         SSECustomerAlgorithm='AES256',
                         SSECustomerKey=p1,
                         SSECustomerKeyMD5=p2
                         )
# import ipdb
# ipdb.set_trace()
# response sample
'''
('base64 encoded md5: ', 'UkqLgnG9CHTfkJn0rZAfKg==')
{u'Body': <botocore.response.StreamingBody object at 0x102c20bd0>, u'AcceptRanges': 'bytes', u'ContentType': 'binary/octet-stream', 'ResponseMetadata': {'HTTPStatusCode': 200, 'HostId': '48v3N8LDgW6jYNwOXsHE6YL+MlC+hBPXv+AZRYR96gwkvo28k9uWACKcSnD915zF1fCFcz5sUNI=', 'RequestId': '03A0C5D16E1708FD'}, u'LastModified': datetime.datetime(2016, 2, 22, 10, 25, 4, tzinfo=tzutc()), u'ContentLength': 19, u'SSECustomerAlgorithm': 'AES256', u'ETag': '"3a1f500a05252138d31ffb8ca9a8f421"', u'SSECustomerKeyMD5': 'UkqLgnG9CHTfkJn0rZAfKg==', u'Metadata': {}}

'''

try:
    s3_file_content = response['Body'].read()
    with open("ololo.jpg", 'wb') as f:
        f.write(s3_file_content)

except Exception as e:
    print(e)
    raise e

