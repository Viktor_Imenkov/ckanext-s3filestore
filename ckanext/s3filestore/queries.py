from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

import ckan.model as model

Base = declarative_base()
session = model.Session()


class ResourceKeys(Base):
    __tablename__ = 'resource_keys'
    id = Column(Integer, primary_key=True, autoincrement=True)
    resource_id = Column(String, nullable=False)
    key = Column(String, nullable=False)


def resource_key_save(resource_id=None, key=''):
    if resource_id:
        resource_key = ResourceKeys(resource_id=resource_id, key=key)
        session.add(resource_key)
        session.commit()
