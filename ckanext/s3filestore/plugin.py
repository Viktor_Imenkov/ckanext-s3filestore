# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from routes.mapper import SubMapper
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
from ckan.lib.navl.validators import not_empty
import ckanext.s3filestore.uploader


def security_types():
    return 'openned', 'less secure', 'secure'


def create_approved_opts():
    user = toolkit.get_action('get_site_user')({'ignore_auth': True}, {})
    print(user)
    context = {'user': user['name']}
    try:
        data = {'id': 'approved_opts'}
        toolkit.get_action('vocabulary_show')(context, data)
    except toolkit.ObjectNotFound:
        data = {'name': 'approved_opts'}
        vocab = toolkit.get_action('vocabulary_create')(context, data)
        for tag in ('yes', 'no'):
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            toolkit.get_action('tag_create')(context, data)


def approved_opts():
    create_approved_opts()
    try:
        tag_list = toolkit.get_action('tag_list')
        opts = tag_list(data_dict={'vocabulary_id': 'approved_opts'})
        return opts
    except toolkit.ObjectNotFound:
        return None


class S3FileStorePlugin(plugins.SingletonPlugin, toolkit.DefaultDatasetForm):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IConfigurable)
    plugins.implements(plugins.IUploader)
    plugins.implements(plugins.IRoutes, inherit=True)
    plugins.implements(plugins.ITemplateHelpers)

    plugins.implements(plugins.IDatasetForm)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')

    # IConfigurable

    def configure(self, config):
        toolkit.add_template_directory(config, 'templates')
        toolkit.add_public_directory(config, 'public')
        # Certain config options must exists for the plugin to work. Raise an
        # exception if they're missing.
        missing_config = "{0} is not configured. Please amend your .ini file."
        config_options = (
            'ckanext.s3filestore.aws_access_key_id',
            'ckanext.s3filestore.aws_secret_access_key',
            'ckanext.s3filestore.aws_bucket_name'
        )
        for option in config_options:
            if not config.get(option, None):
                raise RuntimeError(missing_config.format(option))

        # Check that options actually work, if not exceptions will be raised
        if toolkit.asbool(
                config.get('ckanext.s3filestore.check_access_on_startup',
                           True)):
            ckanext.s3filestore.uploader.BaseS3Uploader().get_s3_bucket(
                config.get('ckanext.s3filestore.aws_bucket_name'))

    # IUploader

    def get_resource_uploader(self, data_dict):
        '''Return an uploader object used to upload resource files.'''
        return ckanext.s3filestore.uploader.S3ResourceUploader(data_dict)

    def get_uploader(self, upload_to, old_filename=None):
        '''Return an uploader object used to upload general files.'''
        return ckanext.s3filestore.uploader.S3Uploader(upload_to,
                                                       old_filename)

    # IRoutes

    def before_map(self, map):
        with SubMapper(map, controller='ckanext.s3filestore.controller:S3Controller') as m:
            # Override the resource download links
            m.connect('resource_download',
                      '/dataset/{id}/resource/{resource_id}/download',
                      action='resource_download')
            m.connect('resource_download',
                      '/dataset/{id}/resource/{resource_id}/download/{filename}',
                      action='resource_download')

            # fallback controller action to download from the filesystem
            m.connect('filesystem_resource_download',
                      '/dataset/{id}/resource/{resource_id}/fs_download/{filename}',
                      action='filesystem_resource_download')

            # Intercept the uploaded file links (e.g. group images)
            m.connect('uploaded_file', '/uploads/{upload_to}/{filename}',
                      action='uploaded_file_redirect')

        return map

    # additional field for resource
    def get_helpers(self):
        return {
            'security_types': security_types,
            'approved_opts': approved_opts
        }

    def _modify_package_schema(self, schema):
        # Add our custom_resource_text metadata field to the schema
        schema['resources'].update({
                # toolkit.get_validator('ignore_missing')
                'security_type': [not_empty]
        })
        schema.update({
            'approve_dataset': [
                toolkit.get_validator('ignore_missing'),
                toolkit.get_validator('ignore_not_sysadmin'),
                toolkit.get_converter('convert_to_tags')('approved_opts')
            ]
        })
        return schema

    def show_package_schema(self):
        schema = super(S3FileStorePlugin, self).show_package_schema()

        schema['resources'].update({
            # toolkit.get_validator('ignore_missing')
            'security_type': [not_empty]
            })
        schema['tags']['__extras'].append(toolkit.get_converter('free_tags_only'))
        schema.update({
            'approve_dataset': [
                toolkit.get_converter('convert_from_tags')('approved_opts'), 
                # toolkit.get_validator('ignore_not_sysadmin'),
                toolkit.get_validator('ignore_missing'),
            ]
        })
        return schema

    def update_package_schema(self):
        schema = super(S3FileStorePlugin, self).update_package_schema()
        # add files from modify_package_schema
        schema = self._modify_package_schema(schema)
        return schema

    def create_package_schema(self):
        schema = super(S3FileStorePlugin, self).create_package_schema()
        # add files from modify_package_schema
        schema = self._modify_package_schema(schema)
        return schema

    def is_fallback(self):
        # Return True to register this plugin as the default handler for
        # package types not handled by any other IDatasetForm plugin.
        return True

    def package_types(self):
        # This plugin doesn't handle any special package types, it just
        # registers itself as the default (above).
        return []
